<?php

// #1
$arr = range(1, 100);
echo count($arr);

// #3
$arr = [1, 2, 3, 4, 5];
$result = in_array(2, $arr);
var_dump($result);

// #4
$arr = [1, 2, 3, 4, 5];
echo array_sum($arr);

// #5
$arr = [1, 2, 3, 4, 5];
echo array_product($arr);

// #6
$arr = [1, 2, 3, 4, 5];
$x = array_sum($arr);
$y = count($arr);
echo $x/$y;

// #7
$arr = range(1, 100);
var_dump($arr);


// #8
$arr = range('a', 'z');
var_dump($arr);

// #9
$arr = range(1,9);
echo implode('-', $arr);

// #10
$arr = range(1, 100);
echo array_sum($arr);

// #11
$arr = range(1, 10);
echo array_product($arr);

// #12
$arr1 = [1,2, 3];
$arr2 = ['a', 'b', 'c'];
$result = array_merge($arr1, $arr2);
var_dump ($result);

// #13
$arr = [1, 2, 3, 4, 5];
$result = array_slice($arr, 1, 3);
var_dump($result);

// #14
$arr = [1, 2, 3, 4, 5];
$result = array_splice($arr, 1, 2);
var_dump($arr);

// #15
$arr = [1, 2, 3, 4, 5];
$result = array_splice($arr, 1, 0, [2, 3, 4]);
var_dump($arr);

// #16
$arr =  [1, 2, 3, 4, 5];
$result = array_splice($arr, 2, 0, ['a', 'b', 'c']);
var_dump($arr);

// #17
$arr = [1, 2, 3, 4, 5];
$x = array_splice($arr, 0, 0, ['a', 'b']) || array_splice($arr, 3, 0, ['c']) | array_splice($arr, 4, 0, ['e']);
var_dump($arr);

// #18
$arr = ['a'=>1, 'b'=>2, 'c'=>3];
$keys = array_keys($arr);
$values = array_values($arr);
var_dump($keys);

// #19
$arr1 =  ['a', 'b', 'c'];
$arr2 = [1, 2, 3];
$result = array_combine($arr1, $arr2);
var_dump($result);

// #20
$arr = ['a'=>1, 'b'=>2, 'c'=>3];
var_dump(array_flip($arr));

// #21
$arr3 = ["a" => 1, "b" => 1, "c" => 2];
$var = array_flip($arr3);
print_r($arr);

// #22
/$arr = ['a', '-', 'b', '-', 'c', '-', 'd'];
var_dump(array_search('c', $arr));

// #23
$array = ['a', '-', 'b', '-', 'c', '-', 'd'];
$search = array_search('-', $array);
// echo $search;
var_dump (array_splice($array, 1));

// #24
$arr = ['a', 'b', 'c', 'd', 'e'];
$replace = array_replace($arr, [0=>'!', 3=>'!!']);
var_dump($replace);

// #25
$arr = ['3'=>'a', '1'=>'c', '2'=>'e', '4'=>'b'];
rsort($arr);
var_dump($arr);
$arr = ['3'=>'a', '1'=>'c', '2'=>'e', '4'=>'b'];
natsort($arr);
var_dump($arr);
$arr = ['3'=>'a', '1'=>'c', '2'=>'e', '4'=>'b'];
ksort($arr);
var_dump($arr);

// #26
$arr = ['a'=>1, 'b'=>2, 'c'=>3];
$key = array_rand($arr);
echo $arr[$key];

// #27
$arr = ['a'=>1, 'b'=>2, 'c'=>3];
var_dump(array_rand($arr));

// #28

$arr = [1, 2, 3, 4, 5];
shuffle($arr);
var_dump($arr);

// #29
$arr = range(1, 25);
shuffle($arr);
var_dump($arr);

// #30
$arr = range('a', 'z');
shuffle($arr);
var_dump($arr);

// #31
$arr = range('a', 'e');
shuffle($arr);
var_dump($arr);

// #32
$arr = ['a', 'b', 'c', 'b', 'a'];
var_dump(array_unique($arr));

// #33
$arr = [1, 2, 3, 4, 5];
array_shift($arr);
print_r($arr);

// #34
$arr =[1, 2, 3, 4, 5];
array_unshift($arr, 0);
array_push($arr, 6);
var_dump($arr);

// #35

$arr = [1, 2, 3, 4, 5, 6, 7, 8];

$str = '';
	while (count($arr) > 0) {
		$str .= array_shift($arr);
		$str .= array_pop($arr);
	}
    echo $str;

// #36
$arr = ['a', 'b', 'c'];
$value = array_pad($arr, 6, '-');
var_dump($value);

// #37
$arr =[];
$value = array_pad($arr, 10, 'x');
var_dump($value);

// #38
$arr = range(1, 20);
$result = array_chunk($arr, 5);
var_dump($result);

// #39
$arr = ['a', 'b', 'c', 'b', 'a'];
var_dump(array_count_values($arr));

// #40
$arr = [1, 2, 3, 4, 5];
$result = array_map('sqrt', $arr);
var_dump($result);

// #41
$arr = ['<b>php</b>', '<i>html</i>'];
$result = array_map('strip_tags', $arr);
var_dump($arr);

// #42
$arr = [' a ', ' b ', ' с '];
$result = array_map('trim', $arr);
var_dump($arr);

// #43
$arr = [1, 2, 3, 4, 5];
$arr2 = [3, 4, 5, 6, 7];
$result = array_intersect($arr, $arr2);
print_r ($result);

// #44
$arr = [1, 2, 3, 4, 5];
$arr2 = [3, 4, 5, 6, 7];
$result = array_diff($arr, $arr2);
var_dump($result);

// #45
$arr = 1234567890;
echo array_sum(str_split($arr));

// #46

$arr2 = range('a', 'z');
$arr = range(1, 26);
$result = array_combine($arr2, $arr);
var_dump($result);

// #47

$arr = range(1, 9);
$result = array_chunk($arr, 3);
var_dump($result);







?>









