<?php
// Ex.1 Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).
require_once 'classes/class Employee.php';

// Ex.2 Создайте объект класса Employee, затем установите его свойства в следующие значения - имя 'john', возраст 25, зарплата 1000.
$user = new Employee;
$user->name = 'John';
$user->age = 25;
$user->salary = 1000;

// Ex.3 Создайте второй объект класса Employee, установите его свойства в следующие значения - имя 'eric', возраст 26, зарплата 2000.
$user2 = new Employee;
$user2->name = 'Eric';
$user2->age = 26;
$user2->salary = 2000;

// Ex.4 Выведите на экран сумму зарплат созданных юзеров.
echo $user->salary + $user2->salary . '<br>';

// Ex.5 Выведите на экран сумму возрастов созданных юзеров.
echo $user->age + $user2->age . '<br>';

// Ex.6 Не подсматривая в мой код реализуйте такой же класс User с методом show().
require_once 'classes/class NewClass.php';
$user = new NewClass;
$user->name='Alex';
$user->age=25;
echo $user->show('hello') . '<br>';

//Ex.7 Сделайте класс Employee2, в котором будут следующие свойства - name, age, salary.
require_once 'classes/class Employee2.php';
$man = new Employee2;
$man->salary = 1000;
$man->age = 20;
$man->name = 'Bob';

$man2 = new Employee2;
$man2->salary = 2000;
$man2->age = 30;
$man2->name = 'John' . '<br>';

//Ex.8 Сделайте в классе Employee2 метод getName, который будет возвращать имя работника.
echo $man->getName() . '<br>';
echo $man2->getName() . '<br>';

//Ex.9 Сделайте в классе Employee2 метод getAge, который будет возвращать возраст работника
echo $man->getAge() . '<br>';
echo $man->getAge() . '<br>';

//Ex.10 Сделайте в классе Employee2 метод getSalary, который будет возвращать зарплату работника
echo $man->getSalary() . '<br>';
echo $man2->getSalary() . '<br>';

//Ex.11 Сделайте в классе Employee2 метод checkAge, который будет проверять то, что работнику больше 18 лет и возвращать true, если это так, и false, если это не так.
echo $man->checkAge() . '<br>';
echo $man2->checkAge() . '<br>';

//Ex.12 Создайте два объекта класса Employee, запишите в их свойства какие-либо значения. С помощью метода getSalary найдите сумму зарплат созданных работников.
echo $man->salary+$man2->salary . '<br>';

//Ex.15 Сделайте класс User, в котором будут следующие свойства - name и age.
require_once 'classes/class User.php';

//Ex.16 Сделайте метод setAge, который параметром будет принимать новый возраст пользователя. (+ in class)

//Ex.17 Создайте объект класса User с именем 'john' и возрастом 25. С помощью метода setAge поменяйте возраст на 30. Выведите новое значение возраста на экран.
$object = new User();
$object->name = 'john';
$object->age = 25;

//Ex.18 Модифицируйте метод setAge так, чтобы он вначале проверял, что переданный возраст больше или равен 18.
// Если это так - пусть метод меняет возраст пользователя, а если не так - то ничего не делает.
$object->setAge(10);
echo $object->age . '<br>';

//Ex.19 Сделайте класс Employee, в котором будут следующие свойства работника - name, salary.
// Сделайте метод doubleSalary, который текущую зарплату будет увеличивать в 2 раза.
require_once 'classes/Employee3.php';
$user = new Employee3;
$user->name = 'John';
$user->salary = 5000;
echo $user->doublesalary() . '<br>';

//Ex.20 Сделайте класс Rectangle, в котором в свойствах будут записаны ширина и высота прямоугольника.
require_once 'classes/RectAngle.php';
$new = new RectAngle();
$new->width = 10;
$new->length = 20;

//Ex.21 В классе Rectangle сделайте метод getSquare, который будет возвращать площадь этого прямоугольника.
echo $new->getSquare() . '<br>';

//Ex.22 В классе Rectangle сделайте метод getPerimeter, который будет возвращать периметр этого прямоугольника.
echo $new->getPerimeter() . '<br>';

//Ex.23 Создайте объект этого класса User проверьте работу методов setAge и addAge
require_once 'classes/User2.php';
$newuser = new User2;
$newuser->age = 25;
$newuser->name = 'Paul';
$newuser->setAge(30);
echo $newuser->age . '<br>';
$newuser->addAge(5);
echo $newuser->age . '<br>';

//Ex.24 Сделайте класс Student со свойствами $name и $course (курс студента, от 1-го до 5-го).
require_once 'classes/Student.php';

//Ex.25 В классе Student сделайте public метод transferToNextCourse, который будет переводить студента на следующий курс.
$student = new Student;
$student->course = 1;
$student->transferToNextCourse();


//Ex.25 Выполните в методе transferToNextCourse проверку на то, что следующий курс не больше 5.
echo $student->course . '<br>';

//Ex.26 Сделайте класс Employee, в котором будут следующие публичные свойства - name, age, salary.
// Сделайте так, чтобы эти свойства заполнялись в конструкторе при создании объекта.
require_once 'classes/Employee4.php';

//Ex.27 Создайте объект класса Employee с именем 'eric', возрастом 25, зарплатой 1000.
$object = new Employee4('Johny', 25, 5000);
$object2 = new Employee4('Tom', 30, 7000);
echo $object->salary + $object2->salary . '<br>';

//Ex.28 Сделайте класс Employee, в котором будут следующие приватные свойства: name, age и salary.
require_once 'classes/Employee5.php';

//Ex.29 Дополните класс Employee приватным методом isAgeCorrect, который будет проверять возраст на корректность (от 1 до 100 лет).
// Этот метод должен использоваться в сеттере setAge перед установкой нового возраста (если возраст не корректный - он не должен меняться).
$user = new Employee5();
$user->setAge(25);
echo $user->getAge() . '<br>';

//Ex.30 Пусть зарплата наших работников хранится в долларах. Сделайте так, чтобы геттер getSalary добавлял в конец числа с зарплатой значок доллара.
// Говоря другими словами в свойстве salary зарплата будет хранится просто числом, но геттер будет возвращать ее с долларом на конце
$user->setSalary(1000);
echo $user->getSalary() . '<br>';

//Ex.31 Сделайте класс Employee,6 в котором будут следующие свойства: name, surname и salary.
require_once 'classes/Employee6.php';

//Ex.32 Сделайте так, чтобы свойства name и surname были доступны только для чтения, а свойство salary - и для чтения, и для записи.
$user = new Employee6('Tom', 'Tomm', 1000 );
$user->setSalary(5000);
echo $user->getSalary() . '<br>';

//Ex.33 Сделайте класс City, в котором будут следующие свойства: name, population (количество населения).
require_once 'classes/City.php';
$citizens = [
    new City('Kiev', 10),
    new City('London', 20),
    new City('Jakarta', 30),
    new City('Warczaw', 40),
    new City('Berlin', 50),
    ];
foreach($citizens as $citizen) {
    echo $citizen->name . '<br>' . $citizen->population . '<br>';
    }

//Ex.34 Реализуйте такой же класс Student.
require_once 'classes/StudentNew.php';
$student = new StudentNew('Michalych');
echo $student->getName() . '<br>';

//Ex.35 Модифицируйте метод transferToNextCourse так, чтобы при переводе на новый курс выполнялась проверка того, что новый курс не будет больше 5.
$student->transferToNextCourse();
$student->transferToNextCourse();
$student->transferToNextCourse();
$student->transferToNextCourse();
$student->transferToNextCourse();
echo $student->getCourse() . '<br>';

//Ex.36 Реализуйте класс Arr. В отличие от моего класса метод add вашего класса параметром должен принимать массив чисел.
// Все числа из этого массива должны добавляться в конец массива $this->numbers.
require_once 'classes/Arr.php';
$arr = new Arr();
$arr->ADD(range(1,5));
var_dump($arr) . '<br />';

//Ex.37 Реализуйте также метод getAvg, который будет находить среднее арифметическое чисел.
echo $arr->getAvg() . '<br />';

//Ex.38 Сделайте класс City, в котором будут следующие свойства - name, foundation (дата основания), population (население). Создайте объект этого класса.
require_once 'classes/City2.php';
$city = new City2('Kiev', 1000, 3000000);

//Ex.39 Пусть дана переменная $props, в которой хранится массив названий свойств класса City.
// Переберите этот массив циклом foreach и выведите на экран значения соответствующих свойств.
$props = ['name', 'foundation', 'population'];
foreach ($props as $prop) {
    echo $city->$prop . '<br>';
}

//Ex.40 Скопируйте мой код класса User и массив $props. В моем примере на экран выводится фамилия юзера. Выведите еще и имя, и отчество
$user = new City2('Иванов', 'Иван', 'Иванович');

$props = ['name', 'foundation', 'population'];
    echo $user->{$props[0]} . '<br>'; //доработать - вывести одной строкой
    echo $user->{$props[1]} . '<br>';
    echo $user->{$props[2]} . '<br>';

//Ex.41 Пусть массив $methods будет ассоциативным с ключами method1 и method2. Выведите имя и возраст пользователя с помощью этого массива.
require_once 'classes/User3.php';
$user3 = new User3('Tom', 25);
$methods = ['method1' => 'getName', 'method2' => 'getAge'];
echo $user3->{$methods['method1']}() . '<br>';
echo $user3->{$methods['method2']}() . '<br>';

//Ex.42 реализуйте такой же класс Arr и вызовите его метод getSum сразу после создания объекта.
require_once 'classes/Arr2.php';
$newarr = new Arr2 (range(1,5));
echo $newarr->getSum() . '<br>';

//Ex.43 Реализуйте такой же класс Arr, методы которого будут вызываться в виде цепочки.
require_once 'classes/Arr3.php';
echo ($value=new Arr3(range(1, 3)))->Add(4)->Push([5,7])->getSum();

//Ex.44 Напишите реализацию методов класса ArrayAvgHelper, заготовки методов которого расположены ниже.
require_once 'classes/ArraySumHelper.php';
$arraysumhelper = new ArraySumHelper();
$arr = range(1, 5);
echo $arraysumhelper->getAvg2($arr) . '<br>';













