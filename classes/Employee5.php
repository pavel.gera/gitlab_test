<?php
class Employee5
{
    private $name;
    private $age;
    private $salary;

    public function getAge()
    {
        return $this->age;
    }
    public function setAge($age)
    {
        if ($this->isAgeCorrect($age)) {
        $this->age = $age;
    }
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function getSalary()
    {
        return $this->salary . '$';
    }
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }
    private function isAgeCorrect($age)
    {
        return $age >= 1 and $age <= 100;
    }


}