<?php
class RectAngle
{
    public $width;
    public $length;
    public function getSquare()
    {
        return ($this->width) * ($this->length);
    }
    public function getPerimeter()
    {
        return ($this->length+$this->width)*2;
    }


}