<?php
class Student
{
    public $name;
    public $course;

    public function transferToNextCourse()
    {
        if($this->isCourseCorrect($this->course))
        $this->course += 1;
    }
    private function isCourseCorrect()
    {
        return $this->course >= 1 and $this->course <=5;
        }
    }

